package br.com.automationdemo.page;

import br.com.automationdemo.core.BasePage;
import br.com.automationdemo.attributes.iframeAttributes;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class IframePage extends BasePage implements iframeAttributes{

    public IframePage open() {
        Selenide.open("/Frames.html");
        return this;
    }
    public void singleFrame(){
        entrarFrame(singleIframe);
        inserirValor(textSingleIframe,"Texto inserido no primeiro frame");
        switchTo().defaultContent();
    }

}
