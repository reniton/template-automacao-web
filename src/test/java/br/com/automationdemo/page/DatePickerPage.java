package br.com.automationdemo.page;

import br.com.automationdemo.attributes.DatePickerAttributes;
import br.com.automationdemo.core.BasePage;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class DatePickerPage extends BasePage implements DatePickerAttributes {

    public DatePickerAttributes open() {
        Selenide.open("/Datepicker.html");
        return this;
    }
    public void datePickerDisabled(){
        clicar(datapickerDisabled);
        selectDate("April", "25", "2024");
    }
    public void validateDate(){
        verificarValue(datapickerDisabled,"04/25/2024");
    }
    public void validateDateEnable(){
        verificarValue(DatapickerEnabled,"04/25/2024");
    }

    public void selectDate(String month, String day, String year) {
        clicar(datapickerDisabled);
        // Clicar no ícone para avançar para o próximo mês até encontrarmos o mês correto
        while (!$(DatePickerAttributes.month).text().equalsIgnoreCase(month)) {
            $(".ui-datepicker-next").click();
        }
        // Clicar no dia desejado
        $(By.xpath("//a[text()='" + day + "']")).click();
    }
    public void datePickerEnabled(){
        inserirValor(DatapickerEnabled,"04/25/2024");
    }

}
