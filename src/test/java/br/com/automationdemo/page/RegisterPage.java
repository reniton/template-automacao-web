package br.com.automationdemo.page;

import br.com.automationdemo.attributes.RegisterAttributes;
import br.com.automationdemo.core.BasePage;
import com.codeborne.selenide.Selenide;

import static com.codeborne.selenide.Selenide.$;

public class RegisterPage extends BasePage implements RegisterAttributes {

    public RegisterPage open() {
        Selenide.open("/Register.html");
        return this;
    }

    public void registerComplete(){
        inserirValor(firstName,"Reniton");
        inserirValor(lastName,"Oliveira");
        inserirValor(address,"123 Street, City");
        inserirValor(email,"teste@gmail.com");
        inserirValor(phone,"61999999999");
        clicar(gender);
        clicar(hobbies);
        clicar(languages);
        clicar(english);
        selecionarLista(skills,"Java");
        //selecionarLista(countries,"Canada");
        clicar(selectCountry);
        inserirValor(".select2-search__field","Japan");
        clicar("//li[@class='select2-results__option select2-results__option--highlighted']");
        clicar(country);
        selecionarLista(year,"1990");
        selecionarLista(month,"February");
        selecionarLista(day,"10");
        inserirValor(password,"Password123");
        inserirValor(confirmPassword,"Password123");
    }

    public void submit(){
        clicar(submitButton);
    }





}
