package br.com.automationdemo.page;

import br.com.automationdemo.attributes.DatePickerAttributes;
import br.com.automationdemo.attributes.SliderAttributes;
import br.com.automationdemo.core.BasePage;
import com.codeborne.selenide.Selenide;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.actions;

public class SliderPage extends BasePage implements SliderAttributes {
    public SliderPage open() {
        Selenide.open("/Slider.html");
        return this;
    }
    public void moveSlider(int percentage){
        // Encontrar o elemento da barra deslizante
        String sliderId = slider; // Use o ID correto do elemento
        int sliderWidth = $(sliderId).getSize().getWidth();
        int xOffset = (int) (sliderWidth * percentage / 100.0) - sliderWidth / 2;
        // Mover a barra deslizante
        actions().dragAndDropBy($(sliderId), xOffset, 0).build().perform();
    }
}
