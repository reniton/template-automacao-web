package br.com.automationdemo.core;

import br.com.automationdemo.config.Configuracao;
import br.com.automationdemo.config.ConfigurationManager;
import br.com.automationdemo.page.DatePickerPage;
import br.com.automationdemo.page.IframePage;
import br.com.automationdemo.page.RegisterPage;
import br.com.automationdemo.page.SliderPage;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.testng.annotations.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;

import static com.codeborne.selenide.Selenide.screenshot;
import static io.qameta.allure.Allure.addAttachment;

public class BaseTest {
    protected static RegisterPage register = new RegisterPage();
    protected static IframePage iframe = new IframePage();
    protected static DatePickerPage date = new DatePickerPage();
    protected static SliderPage slider = new SliderPage();
    protected static Configuracao dados = ConfigurationManager.getConfiguration();

    @BeforeClass
    @Parameters({"browser"})
    public void start(String browser) {

        if (browser.equals("Chrome")) {
            Configuration.browser = dados.browserChrome();
        } else if (browser.equals("Firefox")) {
            Configuration.browser = dados.browserFirefox();
        }
        //Configuration.driverManagerEnabled = false;
        Configuration.holdBrowserOpen = true;
        Configuration.headless = true;
        Configuration.baseUrl = dados.url();
        Configuration.timeout = dados.timeout();
        Configuration.browserSize = "1366x768";
    }


    public static void finish() {

        //Tira print pelo selenide
        String print = screenshot("print");
        //Transforma a imagem em binário para anexar ao report  do Allure
        try {
            //Obter a imagem em mémoria
            BufferedImage bimage;
            bimage = ImageIO.read(new File(print));
            // Saida da imagem em binário.
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            //Preparar a saida da imagem em um array de byte, definindo o tipo para a imagem
            ImageIO.write(bimage, "png", baos);
            //Pega a imagem em binÃ¡rio e transforma em array de byte
            byte[] printFinal = baos.toByteArray();
            //anexa a imagem ao Allure para apresentar no relatório.
            addAttachment("Evidência", new ByteArrayInputStream(printFinal));

             WebDriverRunner.driver().close();

        } catch (Exception ex) {
            System.out.println("Deu erro ao anexar o print ao Allure : (. Trace =>" + ex.getMessage());
        }


    }

}
