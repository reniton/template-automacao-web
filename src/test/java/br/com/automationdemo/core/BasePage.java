package br.com.automationdemo.core;

import com.codeborne.selenide.*;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.NoSuchElementException;

import java.io.File;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;


public class BasePage {

    /* -----------------------------> Javascript <---------------------------------  */
//    public String loginComJavascript(String url, String login, String password) {
//        Credentials credentials = new Credentials(login, password);
//        Selenide.open(url, AuthenticationType.BASIC, credentials);
//        return null;
//    }
    /* -----------------------------> Javascript <---------------------------------  */
    public String loginComJavascript(String url, String login, String password) {
        try{

        }catch (Exception e){

        }
        //  Credentials credentials = new Credentials(login, password);
        //  Selenide.open(url, AuthenticationType.BASIC, credentials);
        return null;
    }

    public BasePage javacriptConfirmar() {
        switchTo().alert().accept();
        return this;
    }

    public BasePage javacriptCancelar() {
        switchTo().alert().dismiss();
        return this;
    }

    public BasePage javacriptEscrever(String valor) {
        switchTo().alert().sendKeys(valor);
        return this;
    }

    public String javacriptObtertexto() {
        return switchTo().alert().getText();
    }

    /* ----------------------------> Clicar <--------------------------------  */
    public BasePage clicarLink(String elemento) {
        $(byLinkText(elemento)).click();
        return this;
    }

    public BasePage clicarBotao(String tagname) {
        $(By.tagName(tagname)).click();
        return this;
    }

    public BasePage clicar(String elemento) {
        try{
            $(elemento).click();
        }catch(InvalidSelectorException e){
            $(byXpath(elemento)).click();
        } catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

//    public BasePage clicarElementoXpath(String elemento) {
//        $(byXpath(elemento)).click();
//        return this;
//    }

    public String recuperarTexto(String elemento) {
        String texto = null;
        try{
            texto = $(elemento).getText();
        }catch (InvalidSelectorException e){
            texto = $(By.xpath(elemento)).getText();
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return texto;
    }

//    public String recuperarTextoPorXpath(String xpath) {
//        String texto = $(By.xpath(xpath)).getText();
//        return texto;
//    }

    public BasePage limpar(String elemento) {
        try{
            $(elemento).clear();
        }catch(InvalidSelectorException e){
            $(byXpath(elemento)).clear();
        } catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /*-------------------------------> hover <----------------------------*/
    public BasePage hover(String elemento) {
        try{
            $(elemento).hover();
        }catch (InvalidSelectorException e){
            $(byXpath(elemento)).hover();
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /*-------------------------------> Selecionar <----------------------------*/

    /**
     * Seleciona um dropdown, uma lista de valores
     *
     * @param elemento O elemento principal que contem todos os itens da lista
     * @param valor    O valor que voce quer selecionar
     */
    public String selecionarLista(String elemento, String valor) {
        try{
            $(elemento).selectOptionContainingText(valor);
        }catch (InvalidSelectorException e) {
            $(byXpath(elemento)).selectOptionContainingText(valor);
        }catch(InvalidElementStateException e){
            log("Elemento do tipo inválido");
        }
        return null;
    }

    public String selecionarlink(String elemento) {
        $(byLinkText(elemento)).click();
        return null;
    }

    public String selecionarRadio(String name, String valor) {
        $(By.name(name)).selectRadio(valor);
        return null;
    }

    public String selecionarCheckbox(String element) {
        try {
            $(element).setSelected(true);
        } catch(InvalidSelectorException e){
            $(byXpath(element)).setSelected(true);
        } catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ element + " ("+e+")");
        }
        return null;
    }

    /*public String selecionarCheckboxXpath(String element) {
        $(By.xpath(element)).setSelected(true);
        return null;
    }*/

    public String deselecionarCheckbox(By element) {
        $(element).setSelected(false);
        return null;
    }

    /**
     * Realiza a importação de um arquivo, que esteja na pasta arquivos em resources
     *
     * @param elemento    o elemento do botão ou link para buscar o arquivo
     * @param nomeArquivo O nome do arquivo que deseja incluir
     */
    public BasePage upload(String elemento, String nomeArquivo) {
        File file = $(elemento).uploadFile(new File("src/main/resources/arquivos/" + nomeArquivo));
        return this;
    }

    public BasePage entrarFrame(String elemento) {
        try{
            switchTo().frame($(elemento));
        }catch (InvalidSelectorException e){
            switchTo().frame(elemento);
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    public BasePage abrirAba(String url) {
        executeJavaScript("window.open('" + url + "','Nova Aba');");
        switchTo().window("Nova Aba");
        open(url);
        return this;
    }

    public BasePage fecharjanela() {
        String janelaPrincipal = getWebDriver().getWindowHandle();
        Selenide.closeWindow();
        for (String janela : getWebDriver().getWindowHandles()) {
            if (!(janela.equals(janelaPrincipal))) {
                switchTo().window(janela);
            }
        }
        return this;
    }

    public BasePage recarregar() {
        Selenide.refresh();
        return this;
    }

    /*  ------------------>   métodos de condições  <----------------------     */


    /**
     * Verifica se um item está desabilitado
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemDesativado(String elemento) {
        $(byXpath(elemento)).shouldBe(disabled);
        return this;
    }

    /**
     * Verifica se um elemento HTML é visível na página web
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemVisivel(String elemento) {
        try{
            $(elemento).shouldBe(visible);
        }catch (InvalidSelectorException e){
            $(byXpath(elemento)).shouldBe(visible);
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica se um elemento HTML existe na página web (Esse item pode está visivel ou não)
     * no lugar de exist pode-se colocar: present
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemExiste(String elemento) {
        try{
            $(elemento).shouldBe(exist);
        }catch (InvalidSelectorException e){
            $(byXpath(elemento)).shouldBe(exist);
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica se um elemento HTML existe na página web (Esse item pode está visivel ou não)
     * no lugar de exist pode-se colocar: present
     *
     * @param elemento o elemento que deseja ser verificado
     */
//    public Boolean verificarItemExistePorXpath(String elemento) {
//        $(byXpath(elemento)).shouldBe(exist);
//        return true;
//    }

    /**
     * Verifica se um elemento HTML não existe na página web
     * no lugar de dissappear pode-se colocar: hidden ou not
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemInvisivel(String elemento) {
        try{
            $(elemento).shouldBe(disappear);
        }catch (InvalidSelectorException e){
            $(byXpath(elemento)).shouldBe(disappear);
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica se um elemento HTML permite somente leitura
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemSomenteLeitura(String elemento) {
        try{
            $(elemento).shouldBe(readonly);
        }catch (InvalidSelectorException e){
            $(byXpath(elemento)).shouldBe(readonly);
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica se um atributo está presente em um elemento HTML
     *
     * @param atrribute o atributo que deseja verificar que existe
     * @param elemento  o elemento que deseja ser verificado
     */
    public BasePage verificarAttribute(String elemento, String atrribute) {

        try {
            $(elemento).shouldNotBe(attribute(atrribute));
        }catch (InvalidSelectorException e){
            $(byXpath(elemento)).shouldNotBe(attribute(atrribute));
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica se o atributo name possui um determinado valor
     *
     * @param elemento o elemento que deseja ser verificado que contem a tag name
     * @param valor    o valor que deseja verificar que a tag name tenha
     */
    public BasePage verificarName(String elemento, String valor) {
        try{
            $(elemento).shouldBe(name(valor));
        }catch (InvalidSelectorException e){
            $(byXpath(elemento)).shouldBe(name(valor));
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica se o atributo value possui um determinado valor
     *
     * @param elemento o elemento que deseja ser verificado que contem a tag value
     * @param valor    o valor que deseja verificar que o atributo value tenha
     */
    public BasePage verificarValue(String elemento, String valor) {
        try{
            $(elemento).shouldBe(value(valor));
        }catch (InvalidSelectorException e){
            $(byXpath(elemento)).shouldBe(value(valor));
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica se o atributo type possui um determinado valor
     *
     * @param elemento o elemento que deseja ser verificado que contem a tag type
     * @param valor    o valor que deseja verificar que o atributo type tenha
     */
    public BasePage verificarType(String elemento, String valor) {
        try{
            $(elemento).shouldBe(type(valor));
        }catch (InvalidSelectorException e){
            $(byXpath(elemento)).shouldBe(type(valor));
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica se o atributo id possui um determinado valor
     *
     * @param elemento o elemento que deseja ser verificado que contem o id
     * @param valor    o valor que deseja verificar que o atributo id tenha
     */
    public BasePage verificarID(String elemento, String valor) {
        try{
            $(elemento).shouldBe(id(valor));
        }catch (InvalidSelectorException e){
            $(byXpath(elemento)).shouldBe(id(valor));
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica se o elemento está vazio
     *
     * @param elemento o elemento que deseja ser verificado
     */
//    public BasePage verificarElementoVazio(String elemento) {
//        $(elemento).shouldBe(empty);
//        return this;
//    }
    public BasePage verificarElementoVazio(String elemento) {
        try{
            $(By.xpath(elemento)).shouldBe(empty);
        }catch (InvalidSelectorException e){
            $(By.xpath(elemento)).shouldBe(empty);
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    public BasePage verificarElementoNaoVazio(String elemento) {
        try{
            $(By.xpath(elemento)).shouldNotBe(empty);
        }catch (InvalidSelectorException e){
            $(By.xpath(elemento)).shouldNotBe(empty);
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica se o atributo class possui um determinado valor
     *
     * @param elemento o elemento que deseja ser verificado que contem a class
     * @param valor    o valor que deseja verificar que o atributo class tenha
     */
    public BasePage verificarClass(String elemento, String valor) {
        $(elemento).shouldBe(cssClass(valor));
        return this;
    }

    /**
     * Verifica se o elemento está selecionado
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemSelecionado(String elemento) {
        try{
            $(elemento).shouldBe(selected);
        }catch (InvalidSelectorException e){
            $(By.xpath(elemento)).shouldBe(selected);
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica se o elemento possui um determinado valor de texto, sem considerar letras maiusculas ou minusculas
     *
     * @param elemento o elemento que deseja ser verificado que contem o texto
     * @param valor    o valor que deseja verificar que o atributo class tenha
     */
//    public BasePage verificarTexto(String elemento, String valor) {
//        $(elemento).shouldBe(text(valor));
//        return this;
//    }

    public BasePage verificarTexto(String elemento, String valor) {
        try{
            $(elemento).shouldBe(text(valor));
        }catch (InvalidSelectorException e){
            $(By.xpath(elemento)).shouldBe(text(valor));
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica se o elemento possui um determinado valor de texto, considerando letras maiusculas ou minusculas
     *
     * @param elemento o elemento que deseja ser verificado que contem o texto
     * @param valor    o valor que deseja verificar que o atributo class tenha
     */
    public BasePage verificarTextoCaseSensitive(String elemento, String valor) {
        try{
            $(elemento).shouldBe(textCaseSensitive(valor));
        }catch (InvalidSelectorException e){
            $(By.xpath(elemento)).shouldBe(textCaseSensitive(valor));
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica se um item está habilitado
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemAtivado(String elemento) {
        try{
            $(elemento).shouldBe(enabled);
        }catch (InvalidSelectorException e){
            $(By.xpath(elemento)).shouldBe(enabled);
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    /**
     * Verifica um texto em uma tabela
     *
     * @param elemento o elemento que deseja ser verificado
     * @param texto    o texto que precisa ser validado na tabela
     */
    public ElementsCollection verificarTextoEmtabela(String elemento, String texto) {
        return $$(By.xpath(elemento)).shouldHave(CollectionCondition.itemWithText(texto));
    }
    /* ---------------------------------->  Fim dos métodos de condições  <------------------------------- */

    /**
     * Arrasta um elemento para um determinado ponto da tela
     *
     * @param elemento o elemento para ser movido
     * @param alvo     onde o elemento tem que ser movido
     */
    public BasePage moverElementosPorXpath(String elemento, String alvo) {
        $(byXpath(elemento)).dragAndDropTo(alvo);
        return this;
    }

    /**
     * Método para incluir um valor em um campo
     *
     * @param elemento O elemento onde deseja incluir um valor
     * @param valor    O valor que voce quer incluir
     */
//    public String inserirValor(String elemento, String valor) {
//        $(elemento).setValue(valor);
//        return null;
//    }

    public BasePage inserirValorComTAB(String elemento, String valor) {
        try{
            $(elemento).setValue(valor).pressTab();
        }catch (InvalidSelectorException e){
            $(byXpath(elemento)).setValue(valor).pressTab();
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }

    public BasePage inserirValor(String elemento, String valor) {
        try{
            $(elemento).setValue(valor);
        }catch (InvalidSelectorException e){
            $(byXpath(elemento)).setValue(valor);
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return this;
    }



    public String log(String texto) {
        System.out.println(texto);
        return null;
    }

    public String enter(String elemento) {
        try{
            $(elemento).pressEnter();
        }
        catch (InvalidSelectorException e){
            $(byXpath(elemento)).pressEnter();
        }catch (NoSuchElementException e){
            log("Erro: Não foi possivel localizar o elemento: "+ elemento + " ("+e+")");
        }
        return null;
    }

    public SelenideElement progressoConcluido(String elemento, String valor) {
        return $(by(elemento, valor)).shouldBe(exist);
    }



}

