package br.com.automationdemo.config;

import org.aeonbits.owner.ConfigCache;

public class ConfigurationManager {
    private ConfigurationManager() {
    }
    public static Configuracao getConfiguration() {
        return ConfigCache.getOrCreate(Configuracao.class);
    }

}
