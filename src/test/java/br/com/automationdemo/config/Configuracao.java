package br.com.automationdemo.config;

import org.aeonbits.owner.Config;

//Informar o nome do arquivo properties
@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "classpath:config.properties"
})
public interface Configuracao extends Config {

    //KEY =  nome da variavel que está no properties
    @Key("browser.chrome")
    //Variavel que identificará a variavel que está no properties
    String browserChrome();

    @Key("browser.firefox")
    String browserFirefox();

    @Key("url")
    String url();

    @Key("timeout")
    int timeout();

}

