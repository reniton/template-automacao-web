package br.com.automationdemo.attributes;

public interface RegisterAttributes {
    String firstName = "//input[@placeholder='First Name']";
    String lastName = "//input[@placeholder='Last Name']";
    String address = "//textarea[@ng-model='Adress']";
    String email = "//input[@ng-model='EmailAdress']";
    String phone = "//input[@ng-model='Phone']";
    String gender = "//input[@value='Male']";
    String hobbies = "//input[@value='Cricket']";
    String languages = "//div[@id='msdd']";
    String english = "//a[contains(text(),'English')]";
    String skills = "//select[@id='Skills']";
    String countries = "#countries";
    String selectCountry = "//span[@role='combobox']";
    String country = "#Skills";
    String year = "//select[@id='yearbox']";
    String month = "//select[@ng-model='monthbox']";
    String day = "//select[@id='daybox']";
    String password = "//input[@id='firstpassword']";
    String confirmPassword = "//input[@id='secondpassword']";
    String submitButton = "#submitbtn";
}
