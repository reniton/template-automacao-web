package br.com.automationdemo.attributes;

public interface DatePickerAttributes {
    String datapickerDisabled = "#datepicker1";
    String month = ".ui-datepicker-month";
    String year = ".ui-datepicker-year";
    String day = "td[data-handler='selectDay']:contains(20)";
    String DatapickerEnabled = "#datepicker2";
}
