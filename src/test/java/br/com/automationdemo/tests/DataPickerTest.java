package br.com.automationdemo.tests;

import br.com.automationdemo.core.BaseTest;
import org.testng.annotations.Test;

public class DataPickerTest extends BaseTest {
    @Test
    public static void dataPickerDesabled(){
        date.open();
        date.datePickerDisabled();
        date.validateDate();
    }
    @Test
    public static void dataPickerEnable(){
        date.open();
        date.datePickerEnabled();
        date.validateDateEnable();
    }
}
